package smart_cm;

import java.util.TreeMap;
import java.util.Map;

/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
public class LogicsImpl implements Logics {

    private static final int DISTANCE_NEAR = 200;
    private static final int DISTANCE_FAR = 400;
    private static final int DISTANCE_MAX_DETECTION = 1000;

    private CommChannel channel;
    private Modality modality;
    private int anglePosition;
    private boolean changeAnglePosition;
    private boolean changeTime;
    private int time;
    private String infoText;
    private Map<Integer, Integer> textArea;

    public LogicsImpl(final CommChannel channel){
        this.channel = channel;
        this.modality = Modality.MODALITY_MANUAL;
        this.anglePosition = 0;
        this.changeAnglePosition = false;
        this.changeTime = false;
        this.infoText = " ";
        this.textArea = new TreeMap<>();
    }

    @Override
    public void setModality(final Modality md) {
        this.modality = md;
        this.infoText = " ";
        this.textArea = new TreeMap<>();
    }

    @Override
    public void setTime(final int seconds) {
        if(this.modality == Modality.MODALITY_SINGLE 
                || this.modality == Modality.MODALITY_AUTO) {
            this.time = seconds;
            this.changeTime = true;
        }
    }

    @Override
    public void setAnglePosition(final int pos) {
        if(this.modality == Modality.MODALITY_MANUAL) {
            this.anglePosition = pos;
            this.changeAnglePosition = true;
        }
    }

    @Override
    public int getAnglePosition() {
        return this.anglePosition;
    }

    @Override
    public int getTime() {
        return this.time;
    }

    @Override
    public boolean isChangeAnglePosition() {
        return changeAnglePosition;
    }

    @Override
    public void setChangeAnglePosition(final boolean changeAnglePosition) {
        this.changeAnglePosition = changeAnglePosition;
    }

    @Override
    public boolean isChangeTime() {
        return changeTime;
    }

    @Override
    public void setChangeTime(final boolean changeTime) {
        this.changeTime = changeTime;
    }

    @Override
    public void setConsoleText(final String text) {
        this.infoText = text;
    }

    @Override
    public String getContentAreaText() {
        final StringBuilder result = new StringBuilder(this.modality.toString().replace("_", ": ") + "\n");
        this.textArea.forEach((key, value) -> {
            switch(this.modality) {
            case MODALITY_MANUAL:
                result.append("In " + key + "°, " + (value <= DISTANCE_MAX_DETECTION ? "detected object " + value + "mm distant" : "no object detected") + "\n");
                break;
            case MODALITY_SINGLE:
            case MODALITY_AUTO:
                result.append("In " + key + "°, " + (value >= DISTANCE_NEAR && value <= DISTANCE_FAR ? "detected object " + value + "mm distant" : "no object detected") + "\n");
                break;
            }
        });
        return result.toString();
    }

    @Override
    public String getInfoText() {
        return this.infoText;
    }

    @Override
    public void addText(final Integer key, final Integer text) {
        this.textArea.put(key, text);
    }

    @Override
    public Modality getModality() {
        return modality;
    }

    @Override
    public void sendMsg(final String text) {
        synchronized(channel) {
            channel.sendMsg(text);
        }
        System.out.println("[OUT] " + text);
    }

}
