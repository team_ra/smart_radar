package smart_cm;

/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
public interface Logics {

    // changes the current modality
    void setModality(Modality md);

    // changes the current scan time
    void setTime(int seconds);

    // changes the current motor position
    void setAnglePosition(int pos);

    // returns the current motor position
    int getAnglePosition();

    // returns the current scan time 
    int getTime();

    // returns true if the the current scan time is changed, false otherwise
    boolean isChangeTime();

    // changes the flag of the changed time
    void setChangeTime(boolean changeTime);

    // changes the flag of the changed motor position
    void setChangeAnglePosition(boolean changeAnglePosition);

    // returns true if the current position of the motor is changed, false otherwise
    boolean isChangeAnglePosition();

    // changes the messages shown in GUI
    void setConsoleText(String text);

    // add received message from GUI in text area
    void addText(Integer key, Integer text);

    // returns the received message from Arduino 
    String getContentAreaText();

    //returns the text to put in infoText, like ALARM!, TRAKING! or like Change time
    String getInfoText();

    // returns the current modality
    Modality getModality();

    // sends the message "text" to Arduino
    void sendMsg(String text);
}
