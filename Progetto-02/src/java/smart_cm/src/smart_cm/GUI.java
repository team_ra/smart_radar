package smart_cm;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.*;

/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
public class GUI {

    private final JFrame gui = new JFrame("Smart Radar");
    private final JPanel panel = new JPanel();
    private final JPanel buttons = new JPanel();
    private final JPanel time = new JPanel();
    private final JPanel degree = new JPanel();
    private final JPanel text = new JPanel();
    private final JPanel infoBox = new JPanel();
    private final JButton manual = new JButton("Manual");
    private final JButton single = new JButton("Single");
    private final JButton auto = new JButton("Auto");
    private final JLabel times = new JLabel("Scan duration");
    private final JLabel valTime = new JLabel("");
    private final JLabel degrees = new JLabel("Position");
    private final JLabel valDegrees = new JLabel("");
    private final JTextArea console = new JTextArea(20, 40);
    private final JSlider slideTime = new JSlider(JSlider.HORIZONTAL, 2, 10, 2);
    private final JSlider slideDegrees = new JSlider(JSlider.HORIZONTAL, 0, 180, 0);
    private final JScrollPane scrollBar = new JScrollPane(this.console);
    private final JLabel infoText = new JLabel(" ");
    private final Logics logics;

    public GUI(final CommChannel channel){
        this.logics = new LogicsImpl(channel);		
        this.manualModality(); // when the application starts, the modality is MANUAL
        this.gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        this.buttons.setLayout(new FlowLayout(FlowLayout.CENTER));
        this.time.setLayout(new FlowLayout(FlowLayout.CENTER));
        this.degree.setLayout(new FlowLayout(FlowLayout.CENTER));
        this.text.setLayout(new FlowLayout(FlowLayout.CENTER));	
        this.scrollBar.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        this.scrollBar.setBounds(10, 11, 455, 249);    

        this.panel.add(buttons);
        this.buttons.add(manual);
        this.buttons.add(single);
        this.buttons.add(auto);

        this.panel.add(time);
        this.time.add(times);
        this.time.add(slideTime);
        this.time.add(valTime);

        this.panel.add(degree);
        this.degree.add(degrees);
        this.degree.add(slideDegrees);
        this.degree.add(valDegrees);

        this.panel.add(infoBox);
        this.infoBox.add(infoText);

        this.panel.add(text);
        this.text.add(scrollBar);

        // changes modality in MANUAL
        this.manual.addActionListener(e -> {
            this.manualModality();
        });

        // changes modality in SINGLE
        this.single.addActionListener(e -> {
            this.singleModality();
        });

        // changes modality in AUTO
        this.auto.addActionListener(e -> {
            this.autoModality();
        });

        // manages the time slider
        this.slideTime.addChangeListener(e -> {
            JSlider source = (JSlider)e.getSource();
            // if the user stops moving the slider
            if (!source.getValueIsAdjusting()) {
                int value = (int)source.getValue();
                this.logics.setTime(value);
                this.valTime.setText("Value: " + value + "s");
                this.logics.setConsoleText("[LAST ACTION] Changed scan duration in " + value + " seconds");
                this.infoText.setText(this.logics.getInfoText());
                this.logics.sendMsg("CHANGE_TIME|"+value);
            }
        });

        // manages the degrees slider
        this.slideDegrees.addChangeListener(e -> {
            JSlider source = (JSlider)e.getSource();
            // if the user stops moving the slider
            if (!source.getValueIsAdjusting()) {
                int value = source.getValue();
                this.logics.setAnglePosition(value);
                this.valDegrees.setText("Value: " + value + "°");
                this.logics.setConsoleText("[LAST ACTION] Changed motor position in " + value + "°");
                this.infoText.setText(this.logics.getInfoText());
                this.logics.sendMsg("CHANGE_MOTOR_POSITION|"+value);
            }
        });

        this.draw();

        this.gui.add(panel);
        this.gui.pack();
        this.gui.setLocationRelativeTo(null);
        this.gui.setVisible(true);
    }

    // redraws and changes the GUI, text included
    public void reDraw() {
        this.draw();
        this.console.setText(this.logics.getContentAreaText());
        this.infoText.setText(this.logics.getInfoText());
    }

    // changes modality in GUI
    public void changeModality(final Modality modality) {
        switch(modality) {
        case MODALITY_MANUAL:
            this.manualModality();
            break;
        case MODALITY_SINGLE:
            this.singleModality();
            break;
        case MODALITY_AUTO:
            this.autoModality();
            break;
        }
    }

    // returns the Logics istance
    public Logics getLogics() {
        return this.logics;
    }

    // draws the GUI
    private void draw() {
        this.slideTime.setMajorTickSpacing(1);
        this.slideTime.setMinorTickSpacing(1);
        this.slideTime.setPaintTicks(true);
        this.slideTime.setPaintLabels(true);

        this.slideDegrees.setMajorTickSpacing(45);
        this.slideDegrees.setMinorTickSpacing(1);
        this.slideDegrees.setPaintTicks(true);
        this.slideDegrees.setPaintLabels(true);

        this.buttons.setBackground(Color.orange);
        this.time.setBackground(Color.orange);
        this.degree.setBackground(Color.orange);
        this.text.setBackground(Color.orange);

        this.manual.setOpaque(true);
        this.single.setOpaque(true);
        this.auto.setOpaque(true);

        this.manual.setBackground(Color.orange);
        this.single.setBackground(Color.orange);
        this.auto.setBackground(Color.orange);

        this.slideDegrees.setBackground(Color.orange);
        this.slideTime.setBackground(Color.orange);
        this.console.setBackground(Color.orange);

        this.manual.setForeground(Color.darkGray);
        this.single.setForeground(Color.darkGray);
        this.auto.setForeground(Color.darkGray);
        this.console.setForeground(new Color(70, 70, 70));
        this.console.setEditable(false);

        Font font = new Font("Calibri", Font.ROMAN_BASELINE, 30);
        this.manual.setFont(font);
        this.single.setFont(font);
        this.auto.setFont(font);
        this.times.setFont(font);
        this.valTime.setFont(font);
        this.degrees.setFont(font);
        this.valDegrees.setFont(font);
        this.slideDegrees.setFont(new Font("Calibri", Font.ROMAN_BASELINE, 17));
        this.slideTime.setFont(new Font("Calibri", Font.ROMAN_BASELINE, 17));
        this.console.setFont(new Font("Calibri", Font.ROMAN_BASELINE, 22));
        this.infoText.setFont(new Font("Calibri", Font.ROMAN_BASELINE, 18));
    }

    // settings for manual modality
    private void manualModality() {
        this.resetTimeLabel();
        this.manual.setEnabled(false);
        this.single.setEnabled(true);
        this.auto.setEnabled(true);
        this.slideTime.setEnabled(false);
        this.slideDegrees.setEnabled(true);
        this.logics.sendMsg("CHANGE_MODALITY|MODALITY_MANUAL");
    }	

    // settings for single modality
    private void singleModality() {
        this.resetDegreesLabel(); 
        this.single.setEnabled(false);
        this.manual.setEnabled(true);
        this.auto.setEnabled(true);
        this.slideTime.setEnabled(true);
        this.slideDegrees.setEnabled(false);
        this.logics.sendMsg("CHANGE_MODALITY|MODALITY_SINGLE");
    }

    // settings for auto modality
    private void autoModality() {
        this.resetDegreesLabel();
        this.auto.setEnabled(false);
        this.manual.setEnabled(true);
        this.single.setEnabled(true);
        this.slideTime.setEnabled(true);
        this.slideDegrees.setEnabled(false);
        this.logics.sendMsg("CHANGE_MODALITY|MODALITY_AUTO");
    }

    // resets the time slider value
    private void resetTimeLabel() {
        this.valTime.setText(" ");

    }

    // resets the degrees slider value
    private void resetDegreesLabel() {
        this.valDegrees.setText(" ");
    }
}
