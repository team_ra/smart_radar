package smart_cm;

/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */

public enum Modality {
    MODALITY_MANUAL, MODALITY_SINGLE, MODALITY_AUTO
}
