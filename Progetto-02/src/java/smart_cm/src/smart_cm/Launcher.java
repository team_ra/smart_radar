package smart_cm;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
public class Launcher {

    public static void main(String[] a) throws Exception {
        final CommChannel channel = new SerialCommChannel("COM3", 9600);
        final GUI gui = new GUI(channel);

        System.out.println("Connessione ad Arduino...");

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    synchronized (channel) {
                        if (channel.isMsgAvailable()) {
                            try {
                                String string = channel.receiveMsg();
                                System.out.println("[IN] " + string);
                                String data[] = string.split(Pattern.quote("|"));
                                switch (data[0]) {
                                case "SCAN_RESULT":
                                    Map<String, Integer> parsedData = this.parseValue(data[1]);
                                    if(gui.getLogics().getModality() == Modality.MODALITY_AUTO) {
                                        if (parsedData.get("controlValue") == 0) {
                                            gui.getLogics().setConsoleText(" ");
                                        } else if(parsedData.get("controlValue") == 1) {
                                            gui.getLogics().setConsoleText("<<<<<<<<<< ALARM >>>>>>>>>>");
                                        } else if(parsedData.get("controlValue") == 2) {
                                            gui.getLogics().setConsoleText("<<<<<<<<<< TRACKING >>>>>>>>>>");
                                        }
                                    }
                                    gui.getLogics().addText(parsedData.get("degrees"), parsedData.get("distance"));
                                    gui.reDraw();
                                    break;
                                case "NEW_MODALITY":
                                    switch (data[1]) {
                                    case "MODALITY_MANUAL":
                                        gui.getLogics().setModality(Modality.MODALITY_MANUAL);
                                        gui.changeModality(Modality.MODALITY_MANUAL);
                                        break;
                                    case "MODALITY_SINGLE":
                                        gui.getLogics().setModality(Modality.MODALITY_SINGLE);
                                        gui.changeModality(Modality.MODALITY_SINGLE);
                                        break;
                                    case "MODALITY_AUTO":
                                        gui.getLogics().setModality(Modality.MODALITY_AUTO);
                                        gui.changeModality(Modality.MODALITY_AUTO);
                                        break;
                                    }
                                    break;
                                }
                            } catch (Exception e) {
                                System.out.println("Errore durante la ricezione del messaggio");
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            // parses the message from Arduino and splits them
            private Map<String, Integer> parseValue(final String value){
                final Map<String, Integer> data = new HashMap<>();
                data.put("controlValue", (int) (Long.parseLong(value) / 1000000000L));
                data.put("degrees", (int) ((Long.parseLong(value) - (1000000000L * data.get("controlValue"))) / 100000L));
                data.put("distance", (int) (Long.parseLong(value) - (1000000000L * data.get("controlValue")) - (data.get("degrees") * 100000L)));
                return data;
            }
        }).start();
    }

}
