/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#include "src/Task.h"
#include "src/serial/SerialTask.h"
#include "src/led/LedTask.h"
#include "src/pir/PirTask.h"
#include "src/servo_motor/MotorTask.h"
#include "src/sonar/SonarTask.h"
#include "src/pot/PotTask.h"
#include "src/button/ButtonTask.h"
#include <avr/sleep.h>

#define LED_LD_PIN 8
#define LED_LA_PIN 9
#define PIR_PIN 3
#define MOTOR_PIN 7
#define SONAR_ECHO_PIN 6
#define SONAR_TRIGGER_PIN 5
#define POT_PIN A0
#define BUTTON_PIN A1
#define BUTTON_INTERRUPT_PIN 2
#define TASKS_CYCLE_TIME 10

// Low power consumption function
void sleep(){
  delay(50);
  // Hibernate function from https://www.instructables.com/id/PIR-Motion-Detector-With-Arduino-Operated-at-Lowes/
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  ADCSRA &= ~(1 << 7);
  sleep_enable();
  sleep_bod_disable();
  attachInterrupt(digitalPinToInterrupt(PIR_PIN), onPirDetect, CHANGE);
  sleep_mode();
  sleep_disable();
  ADCSRA |= (1 << 7);
  detachInterrupt(digitalPinToInterrupt(PIR_PIN));
}

SerialTask serialTask;
LedTask ledTask(LED_LD_PIN, LED_LA_PIN);
PirTask pirTask(PIR_PIN, sleep);
MotorTask motorTask(MOTOR_PIN, TASKS_CYCLE_TIME);
SonarTask sonarTask(SONAR_ECHO_PIN, SONAR_TRIGGER_PIN);
PotTask potTask(POT_PIN);
ButtonTask buttonTask(BUTTON_PIN);

// the first modality is MANUAL
Modality modality = MODALITY_MANUAL;

// the first status for each modality is BEFORE SCAN,
Status status = STATUS_BEFORE_SCAN;

/*
 * MANUAL: used to save the position of the servo, and the scan result to send.
 * AUTO:   used to save the position, the control value (0,1,2) and also the detected object distance
 * SINGLE: this variable is used to save the position of the servo, and also the detected object distance
 */
unsigned long int parameter = 0;

// used to schedule the tasks
unsigned long int startTicks;

// used to manage button interrupt
bool buttonPressed = false;

// init all tasks used in the program and manage the interrupt of the button
void setup(){
  Serial.begin(9600);
  serialTask.init(&modality, &status, &parameter);
  ledTask.init(&modality, &status, &parameter);
  pirTask.init(&modality, &status, &parameter);
  motorTask.init(&modality, &status, &parameter);
  sonarTask.init(&modality, &status, &parameter);
  potTask.init(&modality, &status, &parameter);
  buttonTask.init(&modality, &status, &parameter);
  buttonTask.setInterruptVariable(&buttonPressed);
  attachInterrupt(digitalPinToInterrupt(BUTTON_INTERRUPT_PIN), onButtonPressed, RISING);
  pinMode(A1, INPUT);
}

// each TAKS_CICLE_TIME ms, all tasks restart
void loop(){
  startTicks = millis();
  serialTask.tick();
  potTask.tick();
  ledTask.tick();
  pirTask.tick();
  motorTask.tick();
  sonarTask.tick();
  buttonTask.tick();
  int del = startTicks + TASKS_CYCLE_TIME - millis();
  if(del > 0){
    delay(del);
  }
}

// button interrupt
void onButtonPressed(){
	buttonPressed = true;
}
void onPirDetect(){
  // useless method, just to wake up Arduino from sleep on pir detection
}
