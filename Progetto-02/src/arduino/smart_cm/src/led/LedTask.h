/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#ifndef __LED_TASK__
    #define __LED_TASK__
    #include "../Task.h"
    #include "LedImpl.h"

    class LedTask: public Task{

        public:
            LedTask(int pinld, int pinla);
			/*
			 * if the modality is SINGLE, the led "ld" has to blink when the sonar detects an object;
			 * if the modality is AUTO, the led "la" has to blink, if the sonar detects an object.
			 */
            void modalityChanged();
			/*
			 * When the modality changes, also the led managed has to change: then if the modality is SINGLE,
			 * the reference led is "ld", otherwise, if the modality is AUTO, the reference led is "la".
			 */
            void doStep();

        private:
            Led* led;
            int pinld, pinla;
		    int blinkTicksCount;
            int controlValue;
		    void blink();
    };

#endif
