/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#include "LedTask.h"
#include "Arduino.h"

LedTask::LedTask(int pinld, int pinla) {
	this->pinld = pinld;
	this->pinla = pinla;
	this->blinkTicksCount = LED_BLINK_TICKS_COUNT;
	this->controlValue = 0;
	this->debugContext = "LedTask";
}

/*
 * if the modality is SINGLE, the led "ld" has to blink when the sonar detects an object;
 * if the modality is AUTO, the led "la" has to blink, if the sonar detects an object.
 */
void LedTask::doStep() {
	switch (this->getModality()) {
		case MODALITY_SINGLE:
			switch (this->getStatus()) {
				// if the sonar detects an object, the led "ld" has to blink until the motor reaches the portion
				case STATUS_MOTOR_LED:
					this->blink();
					break;
				// if the sonar doesn't detect an object and the led is on, the led has to switch off
				default:
					if(this->led->isOn()){
						this->led->switchOff();
						this->blinkTicksCount = LED_BLINK_TICKS_COUNT;
					}
			}
			break;
		case MODALITY_AUTO:
			switch (this->getStatus()) {
				// switch off the led "la", save control value and restore parameter and blinkTicksCount
				case STATUS_BEFORE_SCAN:
					this->controlValue = this->getControlValue(true);
					this->setParameter(this->getRealParameter());
					this->blinkTicksCount = LED_BLINK_TICKS_COUNT;
					this->led->switchOff();
					break;
				// if the sonar detected an object during the last scan, the led "la" has to blink.
				case STATUS_MOTOR:
					if (this->controlValue == 1){
						this->blink();
					}
					break;
			}
			break;
	}
}

/*
 * When the modality changes, also the led managed has to change: then if the modality is SINGLE,
 * the reference led is "ld", otherwise, if the modality is AUTO, the reference led is "la".
 */
void LedTask::modalityChanged() {
	// if the modality changes, but one led id still on, the led switches off
	if(this->led != NULL){
		this->led->switchOff();
	}
	switch (this->getModality()) {
		// in modality SINGLE, only the led "ld" is used
		case MODALITY_SINGLE:
			this->led = new LedImpl(pinld);
			break;
		// in modality AUTO, only the led "la" is used
		case MODALITY_AUTO:
			this->led = new LedImpl(pinla);
			break;
	}
}

void LedTask::blink(){
	if (this->blinkTicksCount == LED_BLINK_TICKS_COUNT) {
		this->led->switchOn();
	} else if (this->blinkTicksCount == LED_BLINK_TICKS_COUNT / 2) {
		this->led->switchOff();
	}
	this->blinkTicksCount--;
	if (this->blinkTicksCount <= 0) {
		this->blinkTicksCount = LED_BLINK_TICKS_COUNT;
	}
  }
