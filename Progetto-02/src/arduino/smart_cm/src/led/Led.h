/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#ifndef __LED__
    #define __LED__

    class Led {

        public:
            virtual void switchOn() = 0;
            virtual void switchOff() = 0;
	        virtual bool isOn() = 0;
    
    };

#endif
