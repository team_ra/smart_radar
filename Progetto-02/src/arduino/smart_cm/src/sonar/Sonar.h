/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#ifndef __SONAR__
    #define __SONAR__

    class Sonar{

        public:
            virtual int getDistance() = 0;

    };

#endif
