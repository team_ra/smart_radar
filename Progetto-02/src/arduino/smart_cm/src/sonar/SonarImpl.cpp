/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#include "SonarImpl.h"
#include "Arduino.h"

SonarImpl::SonarImpl(int echoPin, int trigPin){
    this->echoPin = echoPin;
    this->trigPin = trigPin;
    pinMode(echoPin, INPUT);
    pinMode(trigPin, OUTPUT);
}

int SonarImpl::getDistance(){
    digitalWrite(this->trigPin, LOW);
    delayMicroseconds(3);
    digitalWrite(this->trigPin, HIGH);
    delayMicroseconds(5);
    digitalWrite(this->trigPin, LOW);

    return (int)(pulseInLong(this->echoPin, HIGH) * SOUND_SPEED / 2000.0);
}
