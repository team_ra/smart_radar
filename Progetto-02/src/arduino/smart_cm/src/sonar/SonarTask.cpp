/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#include "SonarTask.h"
#include "Arduino.h"

SonarTask::SonarTask(int echoPin, int trigPin) {
	this->sonar = new SonarImpl(echoPin, trigPin);
	this->debugContext = "SonarTask";
}

// when the motor reaches a portion, the sonar starts to scan
void SonarTask::doStep() {
  	switch (this->getModality()) {
		case MODALITY_SINGLE:
			// if the motor is in a portion, the sonar starts to scan
			if (this->getStatus() == STATUS_SONAR_SCAN) {
				this->saveParameter();
			// if the scan is over and the distance of the detected object is between Dnear and Dfar, the led has to blink
			} else if (this->getStatus() == STATUS_RESTORE_PARAMETER && this->savedParameter != 0){
				unsigned long int objectDistance = this->getParameter() - (((int)(this->getParameter() / 100000L)) * 100000L);
				if (objectDistance >= DISTANCE_NEAR && objectDistance <= DISTANCE_FAR) {
					this->setStatus(STATUS_MOTOR_LED);
				} else {
					this->setStatus(STATUS_MOTOR);
				}
				this->setParameter(this->savedParameter);
			}
			break;
		case MODALITY_MANUAL:
			switch (this->getStatus()) {
				// if the motor is in a portion, the sonar starts to scan
				case STATUS_SONAR_SCAN:
					this->saveParameter();
					break;
				// send to GUI the scan result
				case STATUS_RESTORE_PARAMETER:
					this->setParameter(this->savedParameter);
					this->setStatus(STATUS_BEFORE_SCAN);
					break;
			}
    		break;
    	case MODALITY_AUTO:
      		switch (this->getStatus()) {
			// if the motor is in a portion, the sonar starts to scan and sends to GUI the scan result
			case STATUS_SONAR_SCAN:
				int distance = this->sonar->getDistance();
				this->setParameter(this->getParameter() + distance);
				if (distance >= DISTANCE_NEAR && distance <= DISTANCE_FAR) {
					this->setParameter(this->getParameter() - this->getControlValue(false) + 1000000000L);
				} else if(distance < DISTANCE_NEAR) {
					this->setParameter(this->getParameter() - this->getControlValue(false) + 2000000000L);
				}else if(this->getControlValue(true) == 2){
					this->setParameter(this->getParameter() - this->getControlValue(false));
				}
				this->setStatus(STATUS_SERIAL_SEND);
				break;
     		}
      	break;
  	}
}

// save in parameter the detected object distance
void SonarTask::saveParameter(){
	if(this->getParameter() >= 1000000000L){
		// if the parameter has been saved in sonar
		this->savedParameter = 0;
		this->setParameter(this->getParameter() + this->sonar->getDistance());
		this->setStatus(STATUS_SERIAL_SEND);
	}else{
		// if this task is the first that saves the parameter
		this->savedParameter = this->getParameter();
		this->setParameter(1000000000L + this->sonar->getDistance());
	}
}
