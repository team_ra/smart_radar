/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#ifndef __SONARTASK__
    #define __SONARTASK__
    #include "../Task.h"
    #include "SonarImpl.h"

    class SonarTask: public Task{

        public:
            SonarTask(int echoPin, int trigPin);
            // when the motor reaches a portion, the sonar starts to scan
            void doStep();

        private:
            Sonar* sonar;
            unsigned long int savedParameter;
			// save in parameter the detected object distance
            void saveParameter();
    };

#endif
