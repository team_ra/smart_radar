/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#ifndef __SONAR_IMPL__
    #define __SONAR_IMPL__
    #include "Sonar.h"
    #define SOUND_SPEED 343.85 // (331.45 + (0.62 * 20))

    class SonarImpl: public Sonar{

        public:
            SonarImpl(int echoPin, int trigPin);
            int getDistance();

        private:
            int trigPin; // sender
            int echoPin; // receiver
    };

#endif
