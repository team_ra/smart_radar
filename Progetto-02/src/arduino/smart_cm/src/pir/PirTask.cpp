/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#include "PirTask.h"
#include "Arduino.h"

PirTask::PirTask(int pin, void (*inoSleepFunction)()) {
    this->pir = new PirImpl(pin);
    this->debugContext = "PirTask";
    this->inoSleepFunction = inoSleepFunction;
}

// in modality SINGLE, when PIR detects a movement, the scan starts
void PirTask::doStep() {
    switch (this->getModality()) {
        case MODALITY_SINGLE:
            switch (this->getStatus()) {
                case STATUS_BEFORE_SCAN:
                    (*this->inoSleepFunction)(); // wait until PIR interrupt occurs and wake up Arduino
                    this->setStatus(STATUS_MOTOR);
                break;
            }
        break;
    }
}
