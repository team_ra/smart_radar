/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#ifndef __PIRTASK__
    #define __PIRTASK__
    #include "../Task.h"
    #include "PirImpl.h"

    class PirTask: public Task{

      public:
          PirTask(int pin, void (*inoSleepFunction)());
          // in modality SINGLE, when PIR detects a movement, the scan starts
          void doStep();

      private:
          Pir* pir;
          void (*inoSleepFunction)();

    };

#endif
