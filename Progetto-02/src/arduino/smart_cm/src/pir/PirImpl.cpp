/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#include "PirImpl.h"
#include "Arduino.h"

PirImpl::PirImpl(int pin){
    this->pin = pin;
    pinMode(pin, INPUT);
}

// return if the PIR detected a movement
bool PirImpl::detectMovement(){
    return digitalRead(this->pin) == HIGH;
}

// return the PIR pin number
int PirImpl::getPin(){
	return this->pin;
}
