/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#ifndef __PIR__
    #define __PIR__

    class Pir{

        public:
            // return if the PIR detected a movement
            virtual bool detectMovement() = 0;
            // return the PIR pin number
            virtual int getPin() = 0;

    };

#endif
