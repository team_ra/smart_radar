/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#ifndef __PIR_IMPL__
    #define __PIR_IMPL__
    #include "Pir.h"

    class PirImpl: public Pir{

        public:
            PirImpl(int pin);
            // return if the PIR detected a movement
            bool detectMovement();
            // return the PIR pin number
			int getPin();

        private:
            int pin;
    };

#endif
