/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#include "MotorTask.h"

MotorTask::MotorTask(int pin, int taskCycleTime) {
	this->motor = new MotorImpl(pin);
	this->motorPosition = 0;
	this->debugContext = "MotorTask";
	this->motorIsOn = false;
	this->timetoWaitBeforeRestart = 0;
	this->taskCycleTime = taskCycleTime;
}

/*
 * AUTO: the motor moves from 0 to 180° and then back from 180° to 0° in two different scans.
 *       When the motor reaches a portion, it stops to allow the sonar scan.
 * SINGLE: the motor moves from 0 to 180°, only after that the PIR detects a movement. When the motor
 *         reaches a portion, it stops to allow the sonar scan. When it reaches 180° the status is
 *         set back to BEFORE_SCAN.
 * MANUAL: the motor moves up to the position chosen by the user.
 */
void MotorTask::doStep() {
	switch (this->getModality()) {
		case MODALITY_SINGLE:
			if (this->getStatus() == STATUS_BEFORE_SCAN || this->getStatus() == STATUS_RESTORE_PARAMETER) {
				if(this->getStatus() == STATUS_RESTORE_PARAMETER && this->savedParameter != 0){
					unsigned long int objectDistance = this->getParameter() - (((int)(this->getParameter() / 100000L)) * 100000L);
					if (objectDistance >= DISTANCE_NEAR && objectDistance <= DISTANCE_FAR) {
						this->setStatus(STATUS_MOTOR_LED);
					} else {
						this->setStatus(STATUS_MOTOR);
					}
					this->setParameter(this->savedParameter);
				}
				if(!this->motorIsOn){
					this->motorIsOn = true;
					this->motor->on();
					this->motor->setPosition(0);
				}
			} else if (this->getStatus() == STATUS_MOTOR || this->getStatus() == STATUS_MOTOR_LED) {
				this->move(MAX_ANGLE, true);
			} else if (this->getStatus() == STATUS_SONAR_SCAN) {
				this->saveParameter();
			}
			break;
		case MODALITY_MANUAL:
			if(this->getStatus() == STATUS_MOTOR){
				if(!this->motorIsOn){
					this->motorIsOn = true;
					this->motor->on();
					this->motor->setPosition(0);
				}
				if(this->motorPosition == this->getParameter()){
					this->setStatus(STATUS_SONAR_SCAN);
				}else{
					if(this->motorPosition < this->getParameter()){
						this->motorPosition++;
					}else{
						this->motorPosition--;
					}
					this->motor->setPosition(this->motorPosition);
				}
			} else if (this->getStatus() == STATUS_SONAR_SCAN) {
				this->saveParameter();
			}
			break;
		case MODALITY_AUTO:
			switch (this->getStatus()) {
				case STATUS_BEFORE_SCAN:
					if(!this->motorIsOn){
						this->motor->on();
						this->motorIsOn = true;
						this->motorPosition = 0;
						this->motor->setPosition(0);
					}
					this->setStatus(STATUS_MOTOR);
					break;
				case STATUS_MOTOR:
					if (this->getControlValue(true) == 2){
						this->setStatus(STATUS_SONAR_SCAN);
					}else{
						if(this->move(MAX_ANGLE * 2, false)){
							this->savedParameter = this->getRealParameter();
							this->setParameter(this->getControlValue(false) + this->getRealMotorPosition() * 100000L);
						}
					}
					break;
				case STATUS_RESTORE_PARAMETER:
					this->setParameter(this->savedParameter + this->getControlValue(false));
					this->setStatus(STATUS_MOTOR);
					break;
			}
			break;
  	}
}

// if modality changes, reset the variables motorPosition, timeWaitForScan and savedParameter
void MotorTask::modalityChanged() {
	this->motorPosition = 0;
	this->timeToWaitForScan = 0;
	this->savedParameter = 0;
}

int MotorTask::getRealMotorPosition(){
	return this->motorPosition <= MAX_ANGLE ? this->motorPosition : (MAX_ANGLE * 2) - this->motorPosition;
}

bool MotorTask::move(int maxPosition, bool stopMotorOnMaxPosition){
	// true if the motor is in portion
	bool ret = false;
	if(millis() >= this->timeToWaitForScan){
		if(this->motorPosition == -1 || (this->motorPosition != 0 && this->motorPosition % MAX_ANGLE == 0)){
		  if(this->motorPosition == maxPosition){
			  this->motorPosition = -1;
			if(stopMotorOnMaxPosition){
			  this->timetoWaitBeforeRestart = millis() + 1000;
			  this->motor->setPosition(0);
		    }
		  }
		  if(stopMotorOnMaxPosition){
			  if(this->timetoWaitBeforeRestart <= millis()){
				  this->motorPosition = 0;
				  this->setStatus(STATUS_BEFORE_SCAN);
			  }
		  }else{
  			  this->motorPosition++;
			  this->setStatus(STATUS_BEFORE_SCAN);
		  }
		}else{
			int realMotorPosition = this->getRealMotorPosition();
			if(realMotorPosition % (int)(MAX_ANGLE / N_ANGLE) == 0){
				this->timeToWaitForScan = millis() + this->getRealParameter() * 1000 / N_ANGLE - this->taskCycleTime;
				this->setStatus(STATUS_SONAR_SCAN);
				ret = true;
			}
			this->motorPosition++;
			this->motor->setPosition(realMotorPosition);
		}
	}
	return ret;
}

// save in parameter the servo current position
void MotorTask::saveParameter(){
	if(this->getParameter() >= 1000000000L){
		// if the parameter has been saved in sonar
		this->savedParameter = 0;
		this->setParameter(this->getParameter() + (this->motorPosition * 100000L));
		this->setStatus(STATUS_SERIAL_SEND);
	}else{
		// if this task is the first that saves the parameter
		this->savedParameter = this->getParameter();
		this->setParameter(1000000000L + (this->getParameter() * 100000L));
	}
}
