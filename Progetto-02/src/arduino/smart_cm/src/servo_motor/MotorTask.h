/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#ifndef __MOTORTASK__
    #define __MOTORTASK__
    #include "../Task.h"
    #include "MotorImpl.h"
    #define MAX_ANGLE 180

    class MotorTask: public Task{
        public:
            MotorTask(int pin, int taskCycleTime);
            // if modality changes, reset the variables motorPosition, timeWaitForScan and savedParameter
            void modalityChanged();
			/*
			 * AUTO: the motor moves from 0 to 180° and then back from 180° to 0° in two different scans.
			 *       When the motor reaches a portion, it stops to allow the sonar scan.
			 * SINGLE: the motor moves from 0 to 180°, only after that the PIR detects a movement. When the motor
			 *         reaches a portion, it stops to allow the sonar scan. When it reaches 180° the status is
			 *         set back to BEFORE_SCAN.
			 * MANUAL: the motor moves up to the position chosen by the user.
			 */
            void doStep();

        private:
            Motor* motor;
            int motorPosition;
            bool motorIsOn;
            int taskCycleTime;
            unsigned long int savedParameter;
            unsigned long int timeToWaitForScan;
            unsigned long int timetoWaitBeforeRestart;
            bool move(int maxPosition, bool forceSonarScan);
            // save in parameter the servo current position
            void saveParameter();
            int getRealMotorPosition();
    };

#endif
