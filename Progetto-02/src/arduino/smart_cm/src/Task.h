/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#ifndef __TASK__
  #define __TASK__
  	#include "Arduino.h"
  	#define DEBUG 0
    #define MAX_TIME 10 // 10 seconds
    #define MIN_TIME 2 // 2 seconds
    #define N_ANGLE 16 // 180° parts
    #define DISTANCE_FAR 400 // mm
    #define DISTANCE_NEAR 200 // mm
	#define LED_BLINK_TICKS_COUNT 10

    enum Modality {
		MODALITY_SINGLE,
		MODALITY_MANUAL,
		MODALITY_AUTO
	};

    enum Status {
		/*
		 * MANUAL: - SerialTask checks if there's a new position and in this case put it in the parameter.
		 * SINGLE: - SerialTask and PotTask: check if the value of the scan time is different and in this
		 * 									 case put it in the parameter;
		 * 		   - LedTask: if the Led "Ld" is on, switches off;
		 * 		   - PirTask: if detects an object, awakens the Arduino and moves to the MOTOR status;
		 * 		   - MotorTask: switches on the servo motor in the position 0.
		 * AUTO:   - SerialTask and PotTask: check if the value of the scan time is different and in this
		 * 									 case put it in the parameter;
		 * 		   - LedTask: at the end of the scan, it saves and deletes the control value and goes to
		 * 					  MOTOR state.
		 * 		   - MotorTask: if the servo is off, it turns on.
		 */
		STATUS_BEFORE_SCAN,

		/*
		 * MANUAL: - MotorTask: moves the motor to the position chosen by the user, once reached, it switches
		 * 						to the SONAR_SCAN status.
		 * SINGLE: - SerialTask and PotTask: check if the value of the scan time is different and in this
		 * 									 case put it in the parameter;
		 * 		   - LedTask: if the Led "Ld" is on, switches off;
		 * 		   - MotorTask: moves the motor up to 180°, if the motor position is in a portion the modality
		 * 						goes to the SONAR status, instead if the motor postion is 180° the modality goes
		 * 						back to the BEFORE_SCAN status.
		 * AUTO:   - LedTask: if the control value is 1 the led LA has to blinking
		 * 		   - MotorTask: moves the motor up to 180° or 0° only if the control value is other than 2,
		 * 						if the motor position is in a portion the modality goes to the SONAR status,
		 * 						instead if the motor postion is 180° the modality goes back to the
		 * 						BEFORE_SCAN status.
		 */
		STATUS_MOTOR,

		/*
		 * SINGLE: - SerialTask and PotTask: check if the value of the scan time is different and in this
		 * 									 case put it in the parameter;
		 * 		   - LedTask: Led "Ld" blinks.
		 * 		   - MotorTask: moves the motor up to 180°, if the motor position is in a portion the modality
		 * 						goes to the SONAR status, instead if the motor postion is 180° the modality goes
		 * 						back to the BEFORE_SCAN status.
		 */
		STATUS_MOTOR_LED,

		/*
		 * MANUAL: - MotorTask: it saves its current positon.
		 * 		   - SonarTask: it scans, saves the distance of the detected object and goes to SERIAL SEND status.
		 * SINGLE: - MotorTask: it saves its current positon.
		 * 		   - LedTask: if the Led "Ld" is on, switches off;
		 * 		   - SonarTask: it scans, saves the distance of the detected object and goes to SERIAL SEND status.
		 * AUTO:   - SonarTask: it scans, saves the distance of the detected object, and if this distance is between
		 * 						"Dnear" and "Dfar" puts the control value to 1, or if the distance is minor of "near"
		 * 						put the control value to 2, otherwise 0.
		 * 						At the end it goes to SERIAL SEND status.
		 */
		STATUS_SONAR_SCAN,

		/*
		 * MANUAL: - SonarTask: it restores the parameter.
		 * SINGLE: - MotorTask: it restores the parameter.
		 * 		   - LedTask: if the Led "Ld" is on, switches off;
		 * 		   - SonarTask: it restores the parameter.
		 * AUTO:   - MotorTask: it restores the parameter.
		 */
		STATUS_RESTORE_PARAMETER,

		/*
		 * MANUAL: - SerialTask: sends messagge to the interface.
		 * SINGLE: - SerialTask: sends messagge to the interface.
		 * 		   - LedTask: if the Led "Ld" is on, switches off;
		 * AUTO:   - SerialTask: sends messagge to the interface.
		 */
		STATUS_SERIAL_SEND
	};

	//This is an abstract class that is used to manage all important shared methods between tasks.
	class Task {

		public:
			String debugContext = "null";
			String debugMessage = "";

			//init all tasks with a shared variables: Modality, Status and Parameter
			void init(Modality* modality, Status* status, unsigned long int* parameter){
				this->modality = modality;
				this->prevModality = (*modality);
				this->status = status;
				this->parameter = parameter;
				this->modalityChanged();
			}

			//this method is used only if the task has to modify if the modality changes
			virtual void modalityChanged(){}

			//this method represents the activity that the Task has to do.
			virtual void doStep() = 0;

			//this method calls the doStep method and also changes the modality.
			void tick(){
				/* ################ BEGIN DEBUG MESSAGE ################ */
				this->debugMessage = this->stringModality();
				this->debugMessage += F(" - ");
				this->debugMessage += this->stringStatus();
				this->debugMessage += F(" - ");
				this->debugMessage += this->getParameter();
				this->debugPrint();
				/* ################ END DEBUG MESSAGE ################ */
				if(this->prevModality != *(this->modality)){
					this->modalityChanged();
					this->prevModality = (*this->modality);
				}
				this->doStep();
			}

			//returns the current modality.
			Modality getModality(){
				return *(this->modality);
			}

			//sets the modality with the passed modality
			void setModality(Modality modality){
				if(this->prevModality != modality){
					*(this->modality) = modality;
					this->setStatus(STATUS_BEFORE_SCAN);
					this->setParameter(0);
					this->prevModality = modality;
					this->modalityChanged();
				}
			}

			//returns the current status of the program.
			Status getStatus(){
				return *(this->status);
			}

			//sets the current status with the passed status.
			void setStatus(Status status){
				*(this->status) = status;
			}

			//returns the current value of the parameter
			unsigned long int getParameter(){
				return *(this->parameter);
			}

			//sets the current parameter with the passed parameter.
			void setParameter(unsigned long int parameter){
				*(this->parameter) = parameter;
			}

			//this is a useful function to debug the program
			void debugPrint(){
				if(DEBUG){
					Serial.print("[");
					Serial.print(this->debugContext);
					Serial.print("] ");
					Serial.println(this->debugMessage);
				}
			}

			//this is a useful function to debug the program
			String stringModality(){
				switch(*this->modality){
					case MODALITY_SINGLE: return F("MODALITY_SINGLE");
					case MODALITY_MANUAL: return F("MODALITY_MANUAL");
					case MODALITY_AUTO: return F("MODALITY_AUTO");
				}
				return F("MODALITY_NULL");
			}

			//this is a useful function to debug the program
			String stringStatus(){
				switch(*this->status){
					case STATUS_BEFORE_SCAN: return F("STATUS_BEFORE_SCAN");
					case STATUS_MOTOR: return F("STATUS_MOTOR");
					case STATUS_MOTOR_LED: return F("STATUS_MOTOR_LED");
					case STATUS_SONAR_SCAN: return F("STATUS_SONAR_SCAN");
					case STATUS_RESTORE_PARAMETER: return F("STATUS_RESTORE_PARAMETER");
					case STATUS_SERIAL_SEND: return F("STATUS_SERIAL_SEND");
				}
				return F("STATUS_NULL");
			}

			//returns the control value form the parameter
			unsigned long int getControlValue(bool realValue){
				int value = (int)(this->getParameter() / 1000000000L);
				return realValue ? value : value * 1000000000L;
			}

			//returns the real value of the parameter
			unsigned long int getRealParameter(){
				return this->getParameter() - (this->getControlValue(false));
			}

	    private:
            Modality* modality;
			Modality prevModality;
            Status* status;
            unsigned long int* parameter;
	  };
#endif
