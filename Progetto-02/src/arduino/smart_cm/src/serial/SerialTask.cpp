/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#include "SerialTask.h"
#include "Arduino.h"

SerialTask::SerialTask() {
	MsgService.init();
	this->debugContext = "SerialTask";
}

/*
 * receive and send messages from and to GUI
 *
 * These messages are a combination of a type and a value separated by |.
 * RECEIVED MESSAGES -> "CHANGE_MODALITY|<modality>": this message is used to change the modality also from GUI.
 * 													  If this task receives this message, in every moment, has
 * 													  to change the modality in <modality>. <modality> can be
 * 													  MODALITY_SINGLE, MODALITY_AUTO or MODALITY_MANUAL.
 * 					 -> "CHANGE_TIME|<time>": this message is used to change the scan time from GUI in both modalities
 * 											  AUTO and SINGLE. If this task receives this message in modality AUTO
 * 											  the current scan time has to be changed in <time>, otherwise if this
 * 											  task receives this message in modality SINGLE, the next scan time
 * 											  has to be changed in <time>.
 * 					 -> "CHANGE_MOTOR_POSITION|<position>": this message is used to change the motor position in
 * 														    modality MANUAL. If this task receives this message
 * 														    the motor position has to be changed.
 * SENT MESSAGES     -> "SCAN_RESULT|<result>" this message is used to show in GUI all scan result in each modality.
 */
void SerialTask::doStep() {
	if (MsgService.isMsgAvailable()) {
		Msg* msg = MsgService.receiveMsg();
		/* ################ BEGIN DEBUG MESSAGE ################ */
		this->debugMessage = F("Ricevuto messaggio: ");
		this->debugMessage += msg->getContent();
		this->debugPrint();
		/* ################ END DEBUG MESSAGE ################ */
		this->msgType = messageType(msg->getContent());
		this->msgValue = messageValue(msg->getContent());
		delete msg;
		this->messageReceived = true;
	}
	//if the received message is "CHANGE_MODALITY|modality_1", the modality change in modality_1,
	if(this->messageReceived && this->msgType == F("CHANGE_MODALITY")){
		this->messageReceived = false;
		if(this->stringModality() != this->msgValue){
			if(this->msgValue == F("MODALITY_SINGLE")){
				this->setModality(MODALITY_SINGLE);
			}
			if(this->msgValue == F("MODALITY_MANUAL")){
				this->setModality(MODALITY_MANUAL);
			}
			if(this->msgValue == F("MODALITY_AUTO")){
				this->setModality(MODALITY_AUTO);
			}
		}
	}else{
		switch (this->getModality()) {
			case MODALITY_SINGLE:
				switch (this->getStatus()) {
					// send the scan result to GUI
					case STATUS_SERIAL_SEND:
						{
							String msgToSend = F("SCAN_RESULT|");
							MsgService.sendMsg(msgToSend + this->getParameter());
							this->setStatus(STATUS_RESTORE_PARAMETER);
						}
						break;
					//i f the received message is "CHANGE_TIME|time_1", the scan time changes in time_1
					case STATUS_SONAR_SCAN:
					case STATUS_RESTORE_PARAMETER:
						break;
					default:
						if (this->messageReceived && this->msgType == F("CHANGE_TIME")) {
							this->messageReceived = false;
							this->setParameter(this->msgValue.toInt());
						}
				}
		     	break;
			case MODALITY_MANUAL:
				switch (this->getStatus()) {
					// if the received_message is "CHANGE_MOTOR_POSITION|position_1"
					case STATUS_BEFORE_SCAN:
						if(this->messageReceived && this->msgType == F("CHANGE_MOTOR_POSITION")) {
							this->messageReceived = false;
							this->setParameter(this->msgValue.toInt());
						}
						this->setStatus(STATUS_MOTOR);
						break;
						// send the scan result to GUI
					case STATUS_SERIAL_SEND:
						{
						String msgToSend = F("SCAN_RESULT|");
						MsgService.sendMsg(msgToSend + this->getParameter());
						this->setStatus(STATUS_RESTORE_PARAMETER);
						}
						break;
				}
				break;
			case MODALITY_AUTO:
				switch (this->getStatus()) {
					// if the received message is "CHANGE_TIME|time_1", the scan time changes in time_1
					case STATUS_BEFORE_SCAN:
						if (this->messageReceived && this->msgType == F("CHANGE_TIME")) {
							this->messageReceived = false;
							this->setParameter(this->msgValue.toInt());
						}
						break;
					// send the scan result to GUI
					case STATUS_SERIAL_SEND:
						{
						String msgToSend = F("SCAN_RESULT|");
						MsgService.sendMsg(msgToSend + this->getParameter());
						this->setStatus(STATUS_RESTORE_PARAMETER);
						}
						break;
				}
				break;
	  	}
  	}
}


// if modality changes, sends the information to GUI
void SerialTask::modalityChanged(){
	String msgToSend = F("NEW_MODALITY|");
	MsgService.sendMsg(msgToSend + this->stringModality());
}

// get message type (first part, before |)
String SerialTask::messageType(String message) {
	String parsedMessage = "";
	for (int i = 0; i < message.length(); i++) {
		if (message[i] == '|') {
			break;
		}
		parsedMessage += message[i];
	}
	return parsedMessage;
}

// get message value (second part, after |)
String SerialTask::messageValue(String message) {
	String parsedMessage = "";
	for (int i = message.length() - 1; i >= 0; i--) {
		if (message[i] == '|') {
			break;
		}
		parsedMessage = message[i] + parsedMessage;
	}
	return parsedMessage;
}
