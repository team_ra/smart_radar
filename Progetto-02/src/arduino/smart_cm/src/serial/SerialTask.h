/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#ifndef __SERIAL_TASK__
    #define __SERIAL_TASK__
    #include "../Task.h"
    #include "MsgService.h"

    //This class manages the sending and receiving message from and to GUI
    class SerialTask: public Task{

        public:
            SerialTask();
            // receive and send message from and to GUI
            void doStep();
            // if modality changes, sends the information to GUI
			void modalityChanged();

		private:
			bool messageReceived;
			String msgType;
			String msgValue;
			// get message type (first part, before |)
        	String messageType(String message);
			// get message value (second part, after |)
        	String messageValue(String message);
    };

#endif
