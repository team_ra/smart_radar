/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#ifndef __BUTTON_MANAGER__
    #define __BUTTON_MANAGER__

    #define BUTTON_NULL 0 // no button pressed
    #define BUTTON_SINGLE 1 // first button pressed (single)
    #define BUTTON_MANUAL 2 // second button pressed (manual)
    #define BUTTON_AUTO 3 // third button pressed (auto)

    class ButtonManager {

        public:
            virtual int buttonPressed() = 0;

    };

#endif
