/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#ifndef __BUTTON_TASK__
    #define __BUTTON_TASK__
    #include "../Task.h"
    #include "ButtonManagerImpl.h"

    class ButtonTask: public Task{

        public:
            ButtonTask(int buttonPin);
            // change modality after user presses one of the buttons
            void doStep();
            // save the interrupt variable
			void setInterruptVariable(bool* interruptVariable);

        private:
            ButtonManager* buttonManager;
			bool* interruptVariable; // shared variable
    };

#endif
