/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#include "ButtonManagerImpl.h"
#include "Arduino.h"

// the parameter of this constructor is an analog pin
ButtonManagerImpl::ButtonManagerImpl(int pin){
	this->pin = pin;
	pinMode(pin, INPUT);
}

// return which button is being pressed
int ButtonManagerImpl::buttonPressed(){
	int read = analogRead(pin);
	if(read > 976){ // usually == 1023
		return BUTTON_SINGLE;
	}
	if(read > 855){ // usually == 930
		return BUTTON_MANUAL;
	}
	if(read > 600){ // usually == 781
		return BUTTON_AUTO;
	}
    return BUTTON_NULL;
}
