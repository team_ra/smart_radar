/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#include "ButtonTask.h"
#include "Arduino.h"

ButtonTask::ButtonTask(int buttonPin) {
	this->buttonManager = new ButtonManagerImpl(buttonPin);
	this->debugContext = "ButtonTask";
}

// save the interrupt variable
void ButtonTask::setInterruptVariable(bool* interruptVariable){
	this->interruptVariable = interruptVariable;
}

// change modality after user presses one of the buttons
void ButtonTask::doStep() {
	if(*this->interruptVariable){
		(*this->interruptVariable) = false;
		switch(this->buttonManager->buttonPressed()){
			case BUTTON_MANUAL:
				this->setModality(MODALITY_MANUAL);
				break;
			case BUTTON_SINGLE:
				this->setModality(MODALITY_SINGLE);
				break;
			case BUTTON_AUTO:
				this->setModality(MODALITY_AUTO);
				break;
		}
	}
}
