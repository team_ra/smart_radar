/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#ifndef __BUTTON_MANAGER_IMPL__
    #define __BUTTON_MANAGER_IMPL__

    #include "ButtonManager.h"

    class ButtonManagerImpl: public ButtonManager {

        public:
            ButtonManagerImpl(int pin);
            // return which button the user is pressing
            int buttonPressed();

        private:
            // the analog pin used to recognize which button has been pressed
            int pin;

    };

#endif
