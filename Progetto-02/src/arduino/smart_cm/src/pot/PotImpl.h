/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#ifndef __POT_IMPL__
    #define __POT_IMPL__
    #include "Pot.h"

    class PotImpl: public Pot{

        public:
            PotImpl(int pin);
            // return the POT position (a value between 0 and 1023)
            unsigned int getPosition();

        private:
            int pin;
    };

#endif
