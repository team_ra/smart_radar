/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#include "PotTask.h"

PotTask::PotTask(int pin) {
    this->pot = new PotImpl(pin);
    this->lastPotPosition = -1;
    this->debugContext = "PotTask";
}

// in modality SINGLE and AUTO, the POT defines the duration of the scan
void PotTask::doStep() {
    if (
        (this->getModality() == MODALITY_SINGLE &&
            (this->getStatus() == STATUS_BEFORE_SCAN ||
          this->getStatus() == STATUS_MOTOR ||
          this->getStatus() == STATUS_MOTOR_LED))
      || (this->getModality() == MODALITY_AUTO &&
          this->getStatus() == STATUS_BEFORE_SCAN)
    ) {
        unsigned long int actualPosition = (this->pot->getPosition() * (MAX_TIME - MIN_TIME) / MAX_POT_VALUE) + MIN_TIME;
        if (actualPosition != this->lastPotPosition) {
            this->setParameter(actualPosition);
            this->lastPotPosition = actualPosition;
        }
    }
}

// reset lastPotPosition variable
void PotTask::modalityChanged(){
	this->lastPotPosition = -1;
}
