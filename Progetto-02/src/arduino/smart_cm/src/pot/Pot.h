/*
 * Made by Amaducci Giada, Desiderio Marco e Teodorani Cecilia
 * Corso di Sistemi Embedded e Internet of Things - A.A. 2019-2020
 * Elaborato 2
 */
#ifndef __POT__
    #define __POT__

    class Pot{

        public:
            // return the POT position (a value between 0 and 1023)
            virtual unsigned int getPosition() = 0;
    };

#endif
